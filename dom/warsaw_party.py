# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup

import requests

print 'WYSZUKIWARKA IMPREZ W WARSZAWIE'
while True:
    month = raw_input('Podaj miesiac (liczbe): ')
    try:
        month = int(month)
    except:
        month = -1
    if month>0 and month <13:
        if month <10:
            month = '0'+str(month)
        else:
            month = str(month)
        break
    print 'Wprowadziles niepoprawny miesiac'
while True:
    day = raw_input('podaj dzien (liczbe): ')
    try:
        day = int (day)
    except:
        day = -1
    if day>0 and ((month=='02' and day<29) or ((month=='01' or month=='03' or month=='05' or month=='07' or month =='08' or month == '10' or month == '12')and day<32) or (day<31)):
        if day<10:
            day = '0'+str(day)
        else:
            day = str(day)
        break
    print 'Wprowadziles niepoprawny dzien'

link = 'http://warszawa.klubowa.pl/wydarzenia,'+day+','+month+',2013.html'
print '\n'
response = requests.get(link)
html = response.text
parsed = BeautifulSoup(html)
mainColumn = parsed.find('div', id='mainCenterColumn')
boxes = mainColumn.find_all('ul','event-info')
if len(boxes) == 0:
    print 'Na ten dzien nie ma jeszcze zaplanowanch zadnych imprez'
for box in boxes:
    try:
        print box.find('li','event-info-title').text.lstrip().rstrip()
        try:
            if not '0' in box.find('li','event-info-hour').text and not '1' in box.find('li','event-info-hour').text and not '2' in box.find('li','event-info-hour').text:
                print 'Czas: Impreza juz sie odbyla'
            else:
                print 'Czas: ',box.find('li','event-info-hour').text.lstrip().rstrip()
        except:
            print 'Czas: Godzina nie podana'
        try:
            print box.find('li','event-info-price').text.lstrip().rstrip()
        except:
            print 'Wstęp: Cena nie podana'
        try:
            place = box.find('li','event-info-description').text.lstrip().rstrip()
            print 'Miejsce:',place
            try:
                base = 'http://maps.googleapis.com/maps/api/geocode/json'
                data = {'address':place, 'sensor':'false'}
                response = requests.get(base, params=data)
                response = response.json()[u'results'][0][u'formatted_address']
                print 'Adres wg. Google Maps:',response
            except:
                print 'Adres wg. Google Maps: nie znaleziono'

        except:
            print 'Miejsce: nie podane'
        print '\n'
    except:
        print ''





