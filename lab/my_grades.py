# -*- encoding: utf-8 -*-

import getpass
import requests
import operator
from bs4 import BeautifulSoup

url = {
    'login': 'https://e.sggw.waw.pl/login/index.php',
    'grades': 'http://e.sggw.waw.pl/grade/report/user/index.php?id=649'
}

username = raw_input('Użytkownik: ')
password = getpass.getpass('Hasło: ')

payload = {
    'username': username,
    'password': password
}

# LogowanieA
session = requests.session()
session.post('https://e.sggw.waw.pl/login/index.php',data=payload, verify=False)
# Pobranie strony z ocenami
response = session.get(
    'http://e.sggw.waw.pl/grade/report/user/index.php?id=649', verify=False)
# Odczyt strony
html = response.text
# Parsowanie strony
parsed = BeautifulSoup(html)
# Scraping
gradesTable = parsed.find('table', 'user-grade')

folderImage1 = gradesTable.find_all('img', title='Kategoria')[1]
firstCategory = folderImage1.find_parent('td').text
notebookImages = gradesTable.find_all('img', title="Zadanie")
firstDictionary = {'':0}
for image in notebookImages:
    name = image.find_parent('td').text
    #print name
    name += " "
    name += image.find_parent('td').findNext().attrs['href']
    if (image.findNext().text=="-"):
        grade = 0.00
    else:
        grade = float(image.findNext().text.replace(',','.'))
    if 'Zadanie' in name:
        firstDictionary[name]=grade
del firstDictionary['']

folderImage2 = gradesTable.find_all('img',title='Kategoria')[2]
secondCategory = folderImage2.find_parent('td').text
tubeImages = gradesTable.find_all('img',title='Warsztaty')
secondDictionary = {'':0}
for image in tubeImages:
    name = image.find_parent('td').text
    name += " "
    name += image.find_parent('td').findNext().attrs['href']
    if (image.findNext().text=="-"):
        grade = 0.00
    else:
        grade = float(image.findNext().text.replace(',','.'))
    secondDictionary[name]=grade
del secondDictionary['']


folderImage3 = gradesTable.find_all('img',title='Kategoria')[3]
thirdCategory = folderImage3.find_parent('td').text
bubbleImages = gradesTable.find_all('img',title='Forum')
thirdDictionary={'':0}
for image in bubbleImages:
    name = image.find_parent('td').text
    name += " "
    name += image.find_parent('td').findNext().attrs['href']
    if (image.findNext().text=="-"):
        grade = 0.00
    else:
        grade = float(image.findNext().text.replace(',','.'))
    thirdDictionary[name]=grade
del thirdDictionary['']
# Sortowanie

firstDictionary =  sorted(firstDictionary.iteritems(),key=operator.itemgetter(1))
secondDictionary = sorted(secondDictionary.iteritems(),key=operator.itemgetter(1))
thirdDictionary = sorted(thirdDictionary.iteritems(),key=operator.itemgetter(1))


# Wyświetlenie posortowanych ocen w kategoriach
print firstCategory
for index in reversed(range(0,len(firstDictionary))):
    print firstDictionary[index][0],firstDictionary[index][1]
print secondCategory
for index in reversed(range(0,len(secondDictionary))):
    print secondDictionary[index][0],secondDictionary[index][1]
print thirdCategory
for index in reversed(range(0,len(thirdDictionary))):
    print thirdDictionary[index][0],thirdDictionary[index][1]
