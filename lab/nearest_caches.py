# -*- encoding: utf-8 -*-

import requests
from pprint import pprint

addr = raw_input('Podaj adres: ')

# Znalezienie współrzędnych adresu
base = 'http://maps.googleapis.com/maps/api/geocode/json'
data = {'address':addr, 'sensor':'false'}
response = requests.get(base, params=data)
x = response.json()[u'results'][0][u'geometry'][u'location'][u'lat']
y = response.json()[u'results'][0][u'geometry'][u'location'][u'lng']
coordinates = str(x)
coordinates+='|'
coordinates+=str(y)
print 'Coordinates of address You entered: ',coordinates

# Znalezienie najbliższych geoskrytek
key = 'LeNLvZLxZH5rWX8RGDAs'
base = 'http://opencaching.pl/okapi/services/caches/search/nearest'
data = {'center': coordinates, 'consumer_key': key}
response = requests.get(base, params=data)
#print response.json()['results']
caches = '|'.join(response.json()['results'])
base = 'http://opencaching.pl/okapi/services/caches/geocaches'
data = { 'cache_codes': caches, 'consumer_key': key}
result = requests.get(base, params=data)
for sth in response.json()['results']:
    print 'Name: ',result.json()[sth][u'name']
    # Wydobycie informacji o lokalizacji skrytek
    x=result.json()[sth][u'location'].split("|")[0]
    y = result.json()[sth][u'location'].split("|")[1]
    # Przydzielenie adresu skrytkom na podstawie ich współrzędnych
    base = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='
    base+=x
    base+=','
    base+=y
    base+='&sensor=false'
    newresponse = requests.get(base)
    try:
        address = newresponse.json()[u'results'][0][u'formatted_address']
    except:
        "unknown"
    # Wyświetlenie wyniku
    print 'Address: ',address
    print '\n'





